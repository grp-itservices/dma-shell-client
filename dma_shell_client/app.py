"""
    PROTOTYPE: REPL-enabled command line client
    for Data Management App.
"""
from contextlib import suppress
from inspect import signature

from click_repl import repl
from dma_client import RESTClient, ClientException, Data, Collection, Action
import rich.repr
from rich.table import Table
import typer
from pathlib import PosixPath
from typing import Optional
import pandas as pd
import sqlitedict
from enum import Enum
import os

APP_CACHE_PATH = os.path.join(os.path.expanduser('~'), '.dma_shell_client')
cache = sqlitedict.SqliteDict(APP_CACHE_PATH, autocommit=True)
DEFAULT_APP_URL = 'https://dma.embl.de/api/v1'
DISPLAY_FORMAT_KEY = 'display_format'


class DisplayFormat(str, Enum):
    table = "table"
    csv = "csv"
    text = "text"


cli = typer.Typer()
data_app = typer.Typer()
collection_app = typer.Typer()
action_app = typer.Typer()
client = RESTClient(DEFAULT_APP_URL, APP_CACHE_PATH)


def rich_table_repr(
    self
):
    rows = []
    fields = vars(self)
    for key in fields:
        if fields[key]:
            rows.append(str(fields[key]))
        else:
            rows.append("")
    return rows


def rich_repr(self) -> rich.repr.Result:
    if hasattr(self, 'name') and self.name:
        yield self.name
    fields = vars(self)
    fields.pop('name', None)
    for key in fields:
        if fields[key]:
            if isinstance(fields[key], int):
                yield key, int(fields[key])
            else:
                yield key, str(fields[key])


setattr(Data, '__rich_repr__', rich_repr)
setattr(Data, '__rich_table_repr__', rich_table_repr)
setattr(Collection, '__rich_repr__', rich_repr)
setattr(Collection, '__rich_table_repr__', rich_table_repr)
setattr(Action, '__rich_repr__', rich_repr)
setattr(Action, '__rich_table_repr__', rich_table_repr)


def register(func, name, help, app):
    """
    Register a command.
    """
    def wrapped(*args, **kwargs):
        try:
            res = func(*args, **kwargs)
            if res:
                if type(res) == list:
                    res_to_display = res
                else:
                    res_to_display = [res]
                display(res_to_display)
            else:
                typer.secho(f'no result', fg=typer.colors.BRIGHT_YELLOW)
        except Exception as exc:
            typer.secho(f'ERROR: { exc }', fg=typer.colors.RED)
    sig = signature(func)
    new_params = []
    for param in sig.parameters.values():
        if param.annotation is PosixPath:
            new_params.append(param.replace(annotation=str))
        elif param.annotation == Optional[PosixPath]:
            new_params.append(param.replace(annotation=Optional[str]))
        else:
            new_params.append(param)
    wrapped.__signature__ = sig.replace(
        parameters=new_params,
    )
    app.command(name=name, help=help)(wrapped)
    return wrapped


def display(res):
    display_format = DisplayFormat.table
    if DISPLAY_FORMAT_KEY in cache:
        display_format = cache[DISPLAY_FORMAT_KEY]
    if len(res) > 0:
        if display_format == DisplayFormat.text:
            rich.print(res)
        elif display_format == DisplayFormat.table:
            first_row = res[0]
            table = Table(*vars(first_row).keys(), title=first_row.__class__.__name__)
            for row in res:
                table.add_row(*row.__rich_table_repr__())
            rich.print(table)
        else:
            rows = []
            header = vars(res[0]).keys()
            rows.append(header)
            for row in res:
                rows.append(row.__rich_table_repr__())
            df = pd.DataFrame(rows[1:], columns=rows[0])
            print(df.to_csv(index=False))
    else:
        typer.secho(f'no result', fg=typer.colors.BRIGHT_YELLOW)


# register and alias client methods
register(client.list_data, 'ls', 'Display list of data', data_app)
register(client.register_data, 'register', 'Register data', data_app)
register(client.register_collection, 'register', 'Register collection', collection_app)
register(client.list_actions, 'ls', 'Display list of actions', action_app)
register(client.request_archive, 'archive', 'Archive action', action_app)
register(client.request_restore, 'restore', 'Restore action', action_app)
register(client.request_handover, 'handover', 'Handover action', action_app)
register(client.request_share, 'share', 'Share action', action_app)
register(client.request_delete, 'delete', 'Delete action', action_app)
cli.add_typer(data_app, name="data", help="DMA data commands")
cli.add_typer(collection_app, name="collection", help="DMA collection commands")
cli.add_typer(action_app, name="action", help="DMA action commands")


@cli.command()
def auth(
    username: str,
    url: str = typer.Option(DEFAULT_APP_URL),
    password: str = typer.Option(..., prompt=True, hide_input=True),
):
    """
    Authenticate with the API.
    """
    with suppress(ClientException):
        client.set_url(url)
        client.authenticate(username, password)
        typer.secho(
            f'Logged in to { url } as { username }.',
            fg=typer.colors.GREEN
        )
        return
    typer.secho('Authentication failed.', fg=typer.colors.RED)


@cli.command()
def shell(ctx: typer.Context):
    """
    Run in an interactive mode.
    """
    repl(ctx)


@cli.command()
def get_url():
    """
    Get active DMA API url.
    """
    typer.secho(
        f'DMA API client for { client.api_url }.',
        fg=typer.colors.GREEN
    )


@cli.command()
def set_url(url: str):
    """
    Set DMA API url.
    """
    client.set_url(url)
    get_url()


@cli.command()
def get_display_format():
    """
    Get display format.
    """
    display_format = DisplayFormat.table
    if DISPLAY_FORMAT_KEY in cache:
        display_format = cache[DISPLAY_FORMAT_KEY]
    typer.secho(
        f'Display format is `{ display_format }`.',
        fg=typer.colors.GREEN
    )


@cli.command()
def set_display_format(display_format: DisplayFormat):
    """
    Set display format.
    """
    cache[DISPLAY_FORMAT_KEY] = display_format
    get_display_format()


@cli.command()
def logout():
    """
    Logout for specific url.
    """
    client.logout()

def start():
    cli()


if __name__ == '__main__':
    start()


