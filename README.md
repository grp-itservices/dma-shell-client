DMA API shell client
==================================================

Purpose of this app is to provide shell client for DMA API.

Local installation
------------------

* Clone the repository:

```
$ git clone git@git.embl.de:grp-itservices/dma-shell-client.git
$ cd dma-shell-client
pip install poetry
```

* Install package dependencies:
```
$ poetry install
```

* Run app with available commands
```
$ poetry run dma-sc --help
```

* Run interactive shell
```
$ poetry run dma-sc shell
```

**Note**: New version of the Client will only be released when a new tag is created from the master branch.
Make sure that the version number in the `pyprojects.toml` file matches the name of the tag created.


Regular installation
--------------------
This Shell Client requires the DMA Python client (0.0.21+) to also be installed. If you do not have the python client installed you can install using:

```
$ pip install dma-client --extra-index-url https://git.embl.de/api/v4/projects/4393/packages/pypi/simple
```

To install the chell client
```
$ pip install dma-shell-client -U --extra-index-url https://git.embl.de/api/v4/projects/5019/packages/pypi/simple
```

Usage examples
--------------

After the python package installation a client command `dma-sc` will be available.
To display list of available DMA commands:

```shell
dma-sc --help
```

To get more info about the command:
```shell
dma-sc command --help
```

You can run interactive shell that allows you to invoke DMA commands 
directly in the shell (with auto-completion and history):

```shell
dma-sc shell
```

To check DMA API URL use command `get-url`:
```shell
dma-sc get-url
```
Default DMA API URL is https://dma.test.embl.de/api/v1.
You can set different one using command:

```shell
dma-sc set-url https://dma.embl.de/api/v1
```

Before you execute any DMA data command you have to authenticate:
```shell
dma-sc auth my_username
```
You should provide proper username and password. Authentication token is preserved
for specific DMA API url so you need to re-authenticate only when the token expired.

Logout from chosen DMA url:
```shell
dma-sc logout
```

You can redirect output of the command to the file (this will not work in interactive shell):
```shell
dma-sc data ls > output.txt
```

You can set different format of the output data (available options: *text*,*csv*,*table*).
Default one is *table*:
```shell
dma-sc set-display-format text
```

To get current format of the output data use:
```shell
dma-sc get-display-format
```

### Data DMA commands

* Register collection
```shell
dma-sc collection register <collection_name> <collection_comment>
```

* List of datasets - also supports search and filter
```shell
dma-sc data ls 
```

* Register dataset
```shell
dma-sc data register <data_path> --collection-id <collection_id>
```

* List of actions - also supports filtering
```shell
dma-sc action ls
```

* Archive action
```shell
dma-sc action archive <data_id> <budget_number> <delete_data> <comment>
```

* Restore action
```shell
dma-sc action restore <data_id> <archive_id> <restore_to>
```

* Handover action
```shell
dma-sc action handover <data_id> <copy_to> <set_owner> <comment> <delete_original> [set_name] [set_collection] [limit_to]
```

* Share action
```shell
dma-sc action share <data_id> <share_with> <write_access> <comment>
```

* Delete action
```shell
dma-sc action delete <data_id>
```


**Note:** DMA client shell preserve cache under user home directory in a file `.dma_shell_client`




